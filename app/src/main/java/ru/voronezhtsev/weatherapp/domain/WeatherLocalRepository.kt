package ru.voronezhtsev.weatherapp.domain

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.models.domain.Weather

interface WeatherLocalRepository {

    fun put(weather: List<Weather>): List<Weather>

    fun findAll(): Single<List<Weather>>

}