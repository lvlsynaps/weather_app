package ru.voronezhtsev.weatherapp.domain

class ForecastInteractor(private val forecastLocalRepository: ForecastLocalRepository) {

    fun getForecast(cityName: String) = forecastLocalRepository.get(cityName)

}