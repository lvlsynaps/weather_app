package ru.voronezhtsev.weatherapp.domain

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.models.domain.Forecast

interface ForecastLocalRepository {

    fun put(forecast: List<Forecast>)

    fun get(cityName: String): Single<List<Forecast>>
}