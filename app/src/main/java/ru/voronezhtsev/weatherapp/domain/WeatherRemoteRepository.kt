package ru.voronezhtsev.weatherapp.domain

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.models.domain.Weather

interface WeatherRemoteRepository {

    fun get(cityId: Long): Single<List<Weather>>

}