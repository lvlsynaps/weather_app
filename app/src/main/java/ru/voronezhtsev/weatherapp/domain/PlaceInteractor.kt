package ru.voronezhtsev.weatherapp.domain

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.models.domain.Weather

class PlaceInteractor(val forecastRemoteRepository: ForecastRemoteRepository,
                      val forecastLocalRepository: ForecastLocalRepository,
                      val weatherRemoteRepository: WeatherRemoteRepository,
                      val weatherLocalRepository: WeatherLocalRepository) {


    fun load(cityId: Long): Single<List<Weather>> {
        return forecastRemoteRepository.get(cityId)
                .map {
                    forecastLocalRepository.put(it)
                }
                .flatMap { loadWeather(cityId) }
    }

    val list: Single<List<Weather>>
        get() = weatherLocalRepository.findAll()

    private fun loadWeather(cityId: Long): Single<List<Weather>> {
        return weatherRemoteRepository.get(cityId)
                .map {
                    weatherLocalRepository.put(it)
                }
    }
}
