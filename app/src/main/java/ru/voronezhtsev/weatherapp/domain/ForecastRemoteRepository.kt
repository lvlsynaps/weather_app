package ru.voronezhtsev.weatherapp.domain

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.models.domain.Forecast

interface ForecastRemoteRepository {

    fun get(cityId: Long): Single<List<Forecast>>

}