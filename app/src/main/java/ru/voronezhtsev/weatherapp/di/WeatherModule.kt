package ru.voronezhtsev.weatherapp.di

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.voronezhtsev.weatherapp.data.db.AppDatabase
import ru.voronezhtsev.weatherapp.data.remote.ForecastService
import ru.voronezhtsev.weatherapp.data.remote.WeatherService
import ru.voronezhtsev.weatherapp.data.repositories.*
import ru.voronezhtsev.weatherapp.domain.*
import ru.voronezhtsev.weatherapp.presentation.MainScreenConverter
import ru.voronezhtsev.weatherapp.presentation.MainScreenViewModelFactory
import ru.voronezhtsev.weatherapp.presentation.forecast.ForecastViewModelFactory
import ru.voronezhtsev.weatherapp.presentation.weatherlist.WeatherListViewModelFactory
import javax.inject.Singleton

@Module
class WeatherModule {

    @Provides
    fun provideWeatherService(retrofit: Retrofit): WeatherService {
        return retrofit.create(WeatherService::class.java)
    }

    @Provides
    fun provideForecastsService(retrofit: Retrofit): ForecastService {
        return retrofit.create(ForecastService::class.java)
    }

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl("http://api.openweathermap.org/data/2.5/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    @Singleton
    @Provides
    fun provideAppDatabase(context: Context): AppDatabase {
        return Room.databaseBuilder(
                context,
                AppDatabase::class.java, "weather").build()
    }


    @Provides
    @Singleton
    fun provideWeatherInteractor(forecastRemoteRepository: ForecastRemoteRepository,
                                 forecastLocalRepository: ForecastLocalRepository,
                                 weatherRemoteRepository: WeatherRemoteRepository,
                                 weatherLocalRepository: WeatherLocalRepository): PlaceInteractor {
        return PlaceInteractor(forecastRemoteRepository, forecastLocalRepository, weatherRemoteRepository, weatherLocalRepository)
    }

    @Provides
    fun provideCityRepository(context: Context): CityRepository {
        return CityRepositoryImpl(context)
    }

    @Singleton
    @Provides
    fun privideMainScreenConverter(): MainScreenConverter {
        return MainScreenConverter()
    }

    @Provides
    fun provideWeatherListViewModelFactory(placeInteractor: PlaceInteractor): WeatherListViewModelFactory {
        return WeatherListViewModelFactory(placeInteractor)
    }

    @Provides
    fun provideMainScreenViewModelFactory(cityRepository: CityRepository,
                                          mainScreenConverter: MainScreenConverter,
                                          placeInteractor: PlaceInteractor): MainScreenViewModelFactory {

        return MainScreenViewModelFactory(cityRepository, mainScreenConverter, placeInteractor)
    }

    @Provides
    fun provideForecastInteractor(forecastLocalRepository: ForecastLocalRepository) = ForecastInteractor(forecastLocalRepository)

    @Provides
    fun provideForecastViewModel(forecastInteractor: ForecastInteractor, mainScreenConverter: MainScreenConverter): ForecastViewModelFactory {
        return ForecastViewModelFactory(forecastInteractor, mainScreenConverter)
    }

    @Provides
    @Singleton
    fun provideForecastLocalRepository(appDatabase: AppDatabase): ForecastLocalRepository {
        return ForecastLocalRepositoryImpl(appDatabase.forecastDao())
    }

    @Provides
    @Singleton
    fun provideForecastRemoteRepository(forecastService: ForecastService): ForecastRemoteRepository {
        return ForecastRemoteRepositoryImpl(forecastService)
    }

    @Provides
    @Singleton
    fun provideWeatherLocalRepository(appDatabase: AppDatabase): WeatherLocalRepository {
        return WeatherLocalRepositoryImpl(appDatabase.weatherDao())
    }

    @Provides
    @Singleton
    fun provideWeatherRemoteRepository(weatherService: WeatherService): WeatherRemoteRepository {
        return WeatherRemoteRepositoryImpl(weatherService)
    }

}