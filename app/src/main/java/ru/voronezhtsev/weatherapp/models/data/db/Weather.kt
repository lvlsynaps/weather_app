package ru.voronezhtsev.weatherapp.models.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Weather(
        @PrimaryKey
        val cityName: String,
        val temperature: Double,
        val iconCode: String,
        val dateTime: Long,
        val description: String
)

