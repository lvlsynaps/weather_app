package ru.voronezhtsev.weatherapp.models.data.db

import androidx.room.Entity

@Entity(primaryKeys = ["cityName", "dateTime"])
data class Forecast(
        val cityName: String,
        val temperature: Double,
        val iconCode: String,
        val dateTime: Long,
        val description: String
)