package ru.voronezhtsev.weatherapp.models.presentation

data class HostScreenModel(val weatherList: List<WeatherModel>, val cityList: List<CityModel>)