package ru.voronezhtsev.weatherapp.data.repositories

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.data.APP_ID
import ru.voronezhtsev.weatherapp.data.remote.WeatherService
import ru.voronezhtsev.weatherapp.domain.WeatherRemoteRepository
import ru.voronezhtsev.weatherapp.models.domain.Weather
import java.util.*
import java.util.Collections.singletonList

class WeatherRemoteRepositoryImpl(val weatherService: WeatherService) : WeatherRemoteRepository {

    override fun get(cityId: Long): Single<List<Weather>> = weatherService.getWeather(cityId.toString(), APP_ID)
            .map {
                singletonList(Weather(it.main.temp - 273.15,
                        it.name, it.weather[0].icon, Date().time, it.weather[0].description))
            }
}

