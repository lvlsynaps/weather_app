package ru.voronezhtsev.weatherapp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.voronezhtsev.weatherapp.models.data.db.Weather

@Dao
interface WeatherDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(weather: Weather)

    @Query("select * from weather")
    fun findAll(): List<Weather>

}