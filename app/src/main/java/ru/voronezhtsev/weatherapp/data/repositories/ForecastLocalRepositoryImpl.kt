package ru.voronezhtsev.weatherapp.data.repositories

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.data.db.ForecastDao
import ru.voronezhtsev.weatherapp.domain.ForecastLocalRepository
import ru.voronezhtsev.weatherapp.models.domain.Forecast
import ru.voronezhtsev.weatherapp.models.data.db.Forecast as DataForecast

class ForecastLocalRepositoryImpl(val forecastDao: ForecastDao) : ForecastLocalRepository {

    override fun put(forecast: List<Forecast>) {
        forecast.forEach {
            forecastDao.save(DataForecast(it.cityName, it.temp, it.iconCode, it.dateTime, it.description))
        }
    }

    override fun get(cityName: String): Single<List<Forecast>> {
        return Single.fromCallable {
            val forecast = mutableListOf<Forecast>()
            forecastDao.find(cityName).forEach {
                forecast.add(Forecast(it.cityName, it.dateTime, it.temperature, it.iconCode, it.description))
            }
            forecast.toList()
        }
    }
}

