package ru.voronezhtsev.weatherapp.data.repositories

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.data.db.WeatherDao
import ru.voronezhtsev.weatherapp.domain.WeatherLocalRepository
import ru.voronezhtsev.weatherapp.models.domain.Weather
import ru.voronezhtsev.weatherapp.models.data.db.Weather as DataWeather

class WeatherLocalRepositoryImpl(val weatherDao: WeatherDao) : WeatherLocalRepository {

    override fun put(weather: List<Weather>): List<Weather> {
        weather.forEach {
            weatherDao.save(DataWeather(it.cityName, it.temp, it.iconCode, it.datetime, it.description))
        }
        return weather
    }

    override fun findAll(): Single<List<Weather>> {
        return Single.fromCallable {
            val result = mutableListOf<Weather>()
            val dataWeather = weatherDao.findAll()
            dataWeather.forEach {
                result.add(Weather(it.temperature, it.cityName, it.iconCode, it.dateTime, it.description))
            }
            result
        }
    }
}