package ru.voronezhtsev.weatherapp.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import ru.voronezhtsev.weatherapp.models.data.db.Forecast
import ru.voronezhtsev.weatherapp.models.data.db.Weather

@Database(entities = [Weather::class, Forecast::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun weatherDao(): WeatherDao

    abstract fun forecastDao(): ForecastDao

}