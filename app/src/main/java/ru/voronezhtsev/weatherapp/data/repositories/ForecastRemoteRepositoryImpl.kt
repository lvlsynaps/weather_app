package ru.voronezhtsev.weatherapp.data.repositories

import io.reactivex.Single
import ru.voronezhtsev.weatherapp.data.APP_ID
import ru.voronezhtsev.weatherapp.data.remote.ForecastService
import ru.voronezhtsev.weatherapp.domain.ForecastRemoteRepository
import ru.voronezhtsev.weatherapp.models.domain.Forecast

class ForecastRemoteRepositoryImpl(private val forecastService: ForecastService) : ForecastRemoteRepository {

    override fun get(cityId: Long): Single<List<Forecast>> {
        return forecastService.getForecast(cityId.toString(), APP_ID)
                .map {
                    val result = mutableListOf<Forecast>()
                    for (forecast in it.forecast) {
                        result.add(Forecast(it.city.name, forecast.dateTime, forecast.main.temp - 273.15,
                                forecast.weather[0].icon, forecast.weather[0].description))
                    }
                    result
                }
    }
}