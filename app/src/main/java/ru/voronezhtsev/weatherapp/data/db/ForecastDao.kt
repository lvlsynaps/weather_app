package ru.voronezhtsev.weatherapp.data.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import ru.voronezhtsev.weatherapp.models.data.db.Forecast

@Dao
interface ForecastDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(forecast: Forecast)

    @Query("select * from forecast where cityName = :cityName order by dateTime")
    fun find(cityName: String): List<Forecast>

}