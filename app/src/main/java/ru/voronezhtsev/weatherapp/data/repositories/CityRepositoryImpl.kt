package ru.voronezhtsev.weatherapp.data.repositories

import android.content.Context
import com.google.gson.Gson
import io.reactivex.Single
import ru.voronezhtsev.weatherapp.domain.CityRepository
import ru.voronezhtsev.weatherapp.models.data.assets.CityData
import ru.voronezhtsev.weatherapp.models.domain.City

class CityRepositoryImpl(private val context: Context) : CityRepository {
    override val list: Single<Array<City>>
        get() = Single.fromCallable {
            val json = context.assets.open(CITY_LIST_FILE).bufferedReader().use { it.readText() }
            val gson = Gson()
            gson.fromJson(json, Array<CityData>::class.java)
        }.map {
            it.map { City(it.id.toLong(), it.name) }.toTypedArray()
        }

    companion object {
        private const val CITY_LIST_FILE = "city.list.short.json"
    }
}
