package ru.voronezhtsev.weatherapp.presentation.weatherlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.voronezhtsev.weatherapp.domain.PlaceInteractor

class WeatherListViewModelFactory(private val placeInteractor: PlaceInteractor) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T = PlacesViewModel(placeInteractor) as T

}
