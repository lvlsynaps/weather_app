package ru.voronezhtsev.weatherapp.presentation

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import ru.voronezhtsev.weatherapp.domain.CityRepository
import ru.voronezhtsev.weatherapp.domain.PlaceInteractor
import ru.voronezhtsev.weatherapp.models.domain.City
import ru.voronezhtsev.weatherapp.models.domain.Weather
import ru.voronezhtsev.weatherapp.models.presentation.CityModel
import ru.voronezhtsev.weatherapp.models.presentation.HostScreenModel


class MainScreenViewModel(private val cityRepository: CityRepository,
                          private val mainScreenConverter: MainScreenConverter,
                          private val placeInteractor: PlaceInteractor) : ViewModel() {

    val hostScreenModelLiveData: MutableLiveData<HostScreenModel> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun loadWeather() {
        val request = Single.zip(cityRepository.list, placeInteractor.list,
                BiFunction<Array<City>, List<Weather>, HostScreenModel> { cities, weather -> HostScreenModel(mainScreenConverter.convert(weather), convertCities(cities)) })
        request.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    hostScreenModelLiveData.value = it
                }, {})
    }

    private fun convertCities(cities: Array<City>): List<CityModel> {
        val result = mutableListOf<CityModel>()
        cities.forEach { result.add(CityModel(it.id, it.name)) }
        return result
    }
}