package ru.voronezhtsev.weatherapp.presentation.weatherlist

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.voronezhtsev.weatherapp.domain.PlaceInteractor
import ru.voronezhtsev.weatherapp.models.presentation.WeatherModel
import ru.voronezhtsev.weatherapp.presentation.MainScreenConverter

class PlacesViewModel(private val placeInteractor: PlaceInteractor) : ViewModel() {

    private val mainScreenConverter: MainScreenConverter = MainScreenConverter()
    val placesLiveData: MutableLiveData<List<WeatherModel>> = MutableLiveData()

    @SuppressLint("CheckResult")
    fun addPlace(city: Long) {
        Single.concat(placeInteractor.load(city), placeInteractor.list)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { placesLiveData.value = mainScreenConverter.convert(it) }
    }
}