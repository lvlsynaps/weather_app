package ru.voronezhtsev.weatherapp.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.voronezhtsev.weatherapp.domain.CityRepository
import ru.voronezhtsev.weatherapp.domain.PlaceInteractor

class MainScreenViewModelFactory(private val cityRepository: CityRepository,
                                 private val mainScreenConverter: MainScreenConverter,
                                 private val placeInteractor: PlaceInteractor) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainScreenViewModel(cityRepository, mainScreenConverter, placeInteractor) as T
    }

}